//
//  CustomCalendarCell.swift
//  MOBI KIET Calendar
//
//  Created by Kunwar Anirudh Singh on 20/08/17.
//  Copyright © 2017 Kunwar Anirudh Singh. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CustomCalendarCell: JTAppleCell {
    
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var DateLabel: UILabel!
}
